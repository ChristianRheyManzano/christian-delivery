<?php
	if(isset($_SESSION['user_type']) && $_SESSION['user_type']==1 && $_SESSION['login'] && $_GET['pages']=='show_routes'){ 
?>

<div style="margin-bottom:20px; margin-top: 20px;">
				<h1> Routes Dashboard </h1>
</div>

		<div style="padding:10px;">

			<table class="table table-striped">

					<thead>
						<tr>
							<th scope="col"> # </th>
							<th scope="col"> Route Name </th>
							<th scope="col"> Route Distance </th>
							<th scope="col"> Route Cost </th>
							<th scope="col"> Status </th>
							<th scope="col"> Action </th>
						</tr>
					</thead>

					<tbody>
						
							
							<?php
								$count=0;
								while($rows = $show_routes->fetch_assoc()){
									extract($rows);
									$count++;
									?>
										<tr>

										<th scope='row'> <?php echo $count; ?> </th>
										<td> <?php echo $dest->dquery($destination_id,'country_name'); ?> </td>
										<td> <?php echo $delivery_time; ?> </td>
										<td> <?php echo $cost; ?> </td>
										<td> <?php echo $status; ?> </td>
										<td> 
											<a href="?pages=edit_routes&id=<?php echo $id;?>" style="margin-right: 5px;" title='click this to edit data'> <i class="fas fa-pencil-alt"></i> </a> 

											<?php if($status == 'active') {?>
											<a href="?pages=status_routes&id=<?php echo $id;?>&status=<?php echo $status;?>" onclick="return confirm('Are you sure you want to Deactivate this Route?')" title='click this to deactivate data'> <i class="fas fa-trash"></i> </a> <?php } ?>

											<?php if($status == 'deactive') {?>
											<a href="?pages=status_routes&id=<?php echo $id;?>&status=<?php echo $status;?>" onclick="return confirm('Are you sure you want to Activate this Route?')" title='click this to activate data'> <i class="fas fa-check"></i> </a> <?php } ?>

										</td>
										</tr>
									<?php
								}

							?>

					</tbody>

			</table>

		</div>



<?php


 } else
 {
 	header('location:?pages=index_page');
 }

?>