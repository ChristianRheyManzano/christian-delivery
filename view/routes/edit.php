<?php
	if(isset($_SESSION['user_type']) && $_SESSION['user_type']==1 && $_SESSION['login'] && $_GET['pages']=='edit_routes' && $_GET['id']!=""){ 
?>
		<div style="margin-bottom:20px; margin-top: 20px;">
				<h1> Edit Route </h1>
		</div>
		<div style="padding:10px;">

		<form id="new_routes" style="width: 50%;">

			<!-- <div class="form-group">
				<label for="routes_name">  Routes Name  </label>
				<input type="text" name="routes_name" id="routes_name" class="form-control" value="<?php echo $name; ?>">
			</div> -->

			<div class="form-group">
				<label for="routes_name">  Routes Destination  </label>
				<select id="routes_name" class="form-control">
					<option value=""> --SELECT COUNTRY </option>
					<?php

						while($rows = $destination->fetch_assoc())
						{
							
							?>

								<option value="<?php echo $rows['id'];?>" <?php echo ($rows['id']==$destination_id) ? 'selected' :''; ?>> <?php echo $rows['country_name'] ?> </option>

							<?php
						}

					?>
				</select>
			</div>

			<!-- <div class="form-group">
				<label for="delivery_time">  Delivery Time  </label>
				<select name="delivery_time" id="delivery_time" class="form-control">

					<option value=""> --Select Delivery Time  </option>
					<option value="9:00am-9:30am" <?php echo $delivery_time == '9:00am-9:30am' ? 'selected':'' ?>> 9:00 am - 9:30 am  </option>
					<option value="9:30am-10:30am" <?php echo $delivery_time == '9:30am-10:30am' ? 'selected':'' ?>> 9:30 am - 10:30 am  </option>
					<option value="01:30pm-02:30pm" <?php echo $delivery_time == '01:30pm-02:30pm' ? 'selected':'' ?>> 01:30 pm - 02:30 pm  </option>
					<option value="04:30pm-05:00pm" <?php echo $delivery_time == '04:30pm-05:00pm' ? 'selected':'' ?>> 04:30 pm - 05:00 pm  </option>

				</select>
			</div> -->

			<div class="form-group">
				<label for="delivery_time">  Distance  </label>
				<input type="text" name="delivery_time" id="delivery_time" class="form-control" value="<?php echo $delivery_time?>">
			</div>

			<div class="form-group">
				<label for="cost">  Delivery Cost  </label>
				<input type="text" name="cost" id="cost" class="form-control" value="<?php echo $cost; ?>">
			</div>

				<!-- <div class="form-group">
					<label for="dist">  Distances  </label>
					<input type="text" name="dist" id="dist" class="form-control" value="<?php echo $distance; ?>">
				</div> -->


			<input type="hidden" name="id" id='routes_id' value="<?php echo $id ?>">
			<button id="routes_edit_btn" class="btn btn-success"> Update </button>

		</form>

		</div>


		<div id="error_msg">
		<div id="error1"> Error : Routes Name Should not be blank</div>
		<div id="error2"> Error: Distant not be blank </div>
		<div id="error3"> Error: Delivery Cost not be blank </div>
		</div>
<?php


 } else
 {
 	header('location:?pages=index_page');
 }

?>