$(document).ready(function(){

	$("#error_msg").hide();


	// for login
	$("#login-btn").click(function(e){
		e.preventDefault();
		var error = '';
		if($("#useremail").val()==""){
			error += $("#error1").html()+"\n";
		}
		if($("#password").val()==""){
			error += $("#error2").html()+"\n";
		}

		if(!error){
				
				// send ajax
				var data = {"useremail":$("#useremail").val(),"password":$("#password").val(),"login_action":"login"};
				ajaxQuery('../controller/login_controller.php',data).done(function(request){
					
					if(request!='error2'){
							location.href=request;
					} else {
							alert('Error: Wrong Credentials!');
					}
				});

		} else  {
			alert(error);
		}
		
	});


	// route-controller

	$("#routes_btn").click(function(e){
		e.preventDefault();
		var error = '';
		//var time =['9:00am-9:30am','9:30am-10:30am','01:30pm-02:30pm','04:30pm-05:00pm'];
		if($("#routes_name").val()==""){
			error += $("#error1").html()+"\n";
		}
		if($("#delivery_time").val()==""){
			error += $("#error2").html()+"\n";
		}
		if($("#cost").val()==""){
			error += $("#error3").html()+"\n";
		}
		// if($("#dist").val()==""){
		// 	error += "Error : Distance should not be blank"+"\n";
		// }
		// if(jQuery.inArray($("#delivery_time").val(),time)<='-1'){
		// 	error +='Error : Invalid time selection value'+'\n';
		// }
		if(!$("#cost").val().match(/^\d+$/)){
			error += 'Error : Cost must have a numeric value'+'\n';
		}
		if(!$("#delivery_time").val().match(/^\d+$/)){
			error += 'Error : Distance must have a numeric value'+'\n';
		}
		// if(!$("#routes_name").val().match(/^[A-Z]/)){
		// 	error += "Error : Routes Name must be UPPERCASE words"+"\n";
		// }
		// if(!$("#routes_name").val().match(/^(PH|JP|ES|DE|AU|CN|KR)+(\W)?(TO)+(\W)?(PH|JP|ES|DE|AU|CN|KR)+$/) && !$("#routes_name").val().match(/^(PH|JP|ES|DE|AU|CN|KR)+(\W)?(TO)+(\W)?(PH|JP|ES|DE|AU|CN|KR)+(\W)?(TO)+(\W)?(PH|JP|ES|DE|AU|CN|KR)+$/)){
		// 	error += "Error : Routes Name Must be Followed Guideline (ex. PH TO AU or PH TO JP TO AU)"+"\n";
		// }

		if(!error){
				
				// send ajax
				var data = {"routes_name":$("#routes_name").val(),"delivery_time":$("#delivery_time").val(),"cost":$("#cost").val(),"action":"save"};
				ajaxQuery('../controller/routes_controller.php',data).done(function(request){
					if(request==1){
								alert('New Routes Successfully Saved');
								window.location.href='?pages=show_routes';
					} 
					else if(request==2){
								alert('Error : This Routes Destination is already been exist!');
					} else {
						alert('Error : INTERNAL SERVER ERROR!');
					}
				});

		} else  {
			alert(error);
		}
		
	});

	$("#routes_edit_btn").click(function(e){
		e.preventDefault();
		var error = '';
		// var time =['9:00am-9:30am','9:30am-10:30am','01:30pm-02:30pm','04:30pm-05:00pm'];
		if($("#routes_name").val()==""){
			error += $("#error1").html()+"\n";
		}
		if($("#delivery_time").val()==""){
			error += $("#error2").html()+"\n";
		}
		if($("#cost").val()==""){
			error += $("#error3").html()+"\n";
		}
		// if($("#dist").val()==""){
		// 	error += "Error : Distance should not be blank"+"\n";
		// }
		// if(jQuery.inArray($("#delivery_time").val(),time)<='-1'){
		// 	error +='Error : Invalid time selection value'+'\n';
		// }
		if(!$("#cost").val().match(/^\d+$/)){
			error += 'Error : Cost must have a numeric value'+'\n';
		}
		if(!$("#delivery_time").val().match(/^\d+$/)){
			error += 'Error : Distance must have a numeric value'+'\n';
		}
		// if(!$("#routes_name").val().match(/^[A-Z]/)){
		// 	error += "Error : Routes Name must be UPPERCASE words"+"\n";
		// }
		// if(!$("#routes_name").val().match(/^(PH|JP|ES|DE|AU|CN|KR)+(\W)?(TO)+(\W)?(PH|JP|ES|DE|AU|CN|KR)+$/) && !$("#routes_name").val().match(/^(PH|JP|ES|DE|AU|CN|KR)+(\W)?(TO)+(\W)?(PH|JP|ES|DE|AU|CN|KR)+(\W)?(TO)+(\W)?(PH|JP|ES|DE|AU|CN|KR)+$/)){
		// 	error += "Error : Routes Name Must be Followed Guideline (ex. PH TO AU or PH TO JP TO AU)"+"\n";
		// }
		if($("#routes_id").val()==''){
			error += 'Error : Undefined Info Data!'+'\n';
		}

		if(!error){
				
				// send ajax
				var data = {"routes_name":$("#routes_name").val(),"delivery_time":$("#delivery_time").val(),"cost":$("#cost").val(),"id":$("#routes_id").val(),"action":"update"};
				ajaxQuery('../controller/routes_controller.php',data).done(function(request){
					if(request==1){
								alert('Update Successfully');
								window.location.href='?pages=show_routes';
					} 
					else if(request==2){
								alert('Error : This Routes Data is already been exist!');
					} else {
						alert('Error : INTERNAL SERVER ERROR!');
					}
				});

		} else  {
			alert(error);
		}
		
	});

	// product controller

	$("#product_btn").click(function(e){
		e.preventDefault();
		var error = '';
		var product = ['shoes','bag','computer','accessories'];
		if($("#product_name").val()==""){
			error += $("#error1").html()+"\n";
		}
		if($("#product_type").val()==""){
			error += $("#error2").html()+"\n";
		}
		if($("#cost").val()==""){
			error += $("#error3").html()+"\n";
		}
		if(jQuery.inArray($("#product_type").val(),product)<='-1'){
			error +='Error : Invalid time selection value'+'\n';
		}
		if(!$("#cost").val().match(/^\d+$/)){
			error += 'Error : Cost must have a numeric value'+'\n';
		}
		if(!$("#origin").val().match(/^\d+$/)){
			error += "Error : Invalid Data Insertion"+"\n";
		}

		if(!error){
				
				// send ajax
				var data = {"name":$("#product_name").val(),"type":$("#product_type").val(),"cost":$("#cost").val(),"origin":$("#origin").val(),"action":"save"};
				ajaxQuery('../controller/product_controller.php',data).done(function(request){
					if(request==1){
								alert('New Product Successfully Saved');
								window.location.href='?pages=show_product';
					} 
					else if(request==2){
								alert('Error : This Product Data is already been exist!');
					} else {
						alert('Error : INTERNAL SERVER ERROR!');
					}
				});

		} else  {
			alert(error);
		}
		
	});

	$("#product_edit_btn").click(function(e){
		e.preventDefault();
		var error = '';
		var product = ['shoes','bag','computer','accessories'];
		if($("#product_name").val()==""){
			error += $("#error1").html()+"\n";
		}
		if($("#product_type").val()==""){
			error += $("#error2").html()+"\n";
		}
		if($("#cost").val()==""){
			error += $("#error3").html()+"\n";
		}
		if(jQuery.inArray($("#product_type").val(),product)<='-1'){
			error +='Error : Invalid time selection value'+'\n';
		}
		if(!$("#cost").val().match(/^\d+$/)){
			error += 'Error : Cost must have a numeric value'+'\n';
		}
		if(!$("#origin").val().match(/^\d+$/)){
			error += "Error : Invalid Data Insertion"+"\n";
		}
		if($("#product_id").val()==''){
			error += 'Error : Undefined Info Data!'+'\n';
		}
		if(!error){
				
				// send ajax
				var data = {"name":$("#product_name").val(),"type":$("#product_type").val(),"cost":$("#cost").val(),"id":$("#product_id").val(),"origin":$("#origin").val(),"action":"update"};
				ajaxQuery('../controller/product_controller.php',data).done(function(request){
					if(request==1){
								alert('Update Successfully');
								window.location.href='?pages=show_product';
					} 
					else if(request==2){
								alert('Error : This Product Data is already been exist!');
					} else {
						alert('Error : INTERNAL SERVER ERROR!');
					}
				});

		} else  {
			alert(error);
		}
		
	});

	//  users

	$("#register_btn").click(function(e){
		e.preventDefault();
		var error = '';
		if($("#fname").val()==""){
			error += "Error : Fullname should not be blank"+"\n";
		}
		if($("#uname").val()==""){
			error += "Error : Username should not be blank"+"\n";
		}
		if($("#email").val()==""){
			error += "Error : Email Address should not be blank"+"\n";
		}
		if($("#country").val()==""){
			error += "Error : Country should not be blank"+"\n";
		}
		if($("#pass").val()==""){
			error += "Error : Password should not be blank"+"\n";
		}
		if($("#cpass").val()==""){
			error += "Error : Confirm Password should not be blank"+"\n";
		}
		if($("#cpass").val()!=$("#pass").val()){
			error += "Error : Password does not match"+"\n";
			$("#cpass").val("");
			$("#pass").val("");
		}
		if(!$("#country").val().match(/^\d+$/)){
			error += "Error : Invalid Data Insertion"+"\n";
		}
		if($("#fname").val().match(/^\W+$/)){
			error += "Error : Fullname should not be blank"+"\n";
		}
		if($("#uname").val().match(/^\W+$/)){
			error += "Error : Username should not be blank"+"\n";
		}
		if($("#pass").val().match(/^\W+$/)){
			error += "Error : Password should not be blank"+"\n";
		}
		if(!$("#email").val().match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)){
			error += 'Error : Email is in wrong format'+'\n';
		}
		if(!error){
				
				// send ajax
				var data = {"fname":$("#fname").val(),"uname":$("#uname").val(),"email":$("#email").val(),"country":$("#country").val(),"pass":$("#pass").val(),"address":$("#address").val(),"action":"save"};
				ajaxQuery('../controller/register_controller.php',data).done(function(request){
					if(request=='success'){
								alert('You Successfully Registered!');
								window.location.href='?pages=index_page';
					} 
					else if(request=='u_exist'){
								alert('Error : Username is already been exist!');
								$("#cpass").val("");
								$("#pass").val("");
					}
					else if(request=='e_exist'){
								alert('Error : Email Address is already been exist!');
								$("#cpass").val("");
								$("#pass").val("");
					} else {
						alert('Error : INTERNAL SERVER ERROR!');
					}
				});

		} else  {
			alert(error);
		}
		
	});


	$("#edit_account_btn").click(function(){
		var error = '';
		if($("#fname").val()==""){
			error += "Error : Fullname should not be blank"+"\n";
		}
		if($("#uname").val()==""){
			error += "Error : Username should not be blank"+"\n";
		}
		if($("#email").val()==""){
			error += "Error : Email Address should not be blank"+"\n";
		}
		if($("#country").val()==""){
			error += "Error : Country should not be blank"+"\n";
		}
		if($("#opass").val()!=""){
			if(!$("#pass").val()){
				error += "Error : New Password should not be blank"+"\n";
			}
			if($("#cpass").val()!=$("#pass").val()){
			error += "Error : New Password does not match"+"\n";
			$("#cpass").val("");
			$("#pass").val("");
			$("#opass").val("");
			}
			if($("#opass").val()==$("#pass").val()){
			error += "Error : New Password must not be equal to Old Password"+"\n";
			$("#cpass").val("");
			$("#pass").val("");
			$("#opass").val("");
			}
		}
		if($("#pass").val()!=""){
			if(!$("#opass").val()){
				error += "Error : Old Password should not be blank"+"\n";
			}
		}
		if(!$("#country").val().match(/^\d+$/)){
			error += "Error : Invalid Data Insertion"+"\n";
		}
		if($("#fname").val().match(/^\W+$/)){
			error += "Error : Fullname should not be blank"+"\n";
		}
		if($("#uname").val().match(/^\W+$/)){
			error += "Error : Username should not be blank"+"\n";
		}
		if($("#pass").val().match(/^\W+$/)){
			error += "Error : Password should not be blank"+"\n";
		}
		if(!$("#email").val().match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)){
			error += 'Error : Email is in wrong format'+'\n';
		}

		if(!error){
				
				// send ajax
				var data = {"fname":$("#fname").val(),"uname":$("#uname").val(),"email":$("#email").val(),"country":$("#country").val(),"pass":$("#pass").val(),"opass":$("#opass").val(),"address":$("#address").val(),"action":"update"};
				ajaxQuery('../controller/register_controller.php',data).done(function(request){
					if(request=='success'){
								alert('Your Account Successfully Updated');
								window.location.href='?pages=users_account';
					} 
					else if(request=='u_exist'){
								alert('Error : Username is already been exist!');
								$("#cpass").val("");
								$("#pass").val("");
								$("#opass").val("");
					}
					else if(request=='e_exist'){
								alert('Error : Email Address is already been exist!');
								$("#cpass").val("");
								$("#pass").val("");
								$("#opass").val("");
					}
					else if(request=='op_wrong'){
								alert('Error : Old Password Does not Match to Current Password');
								$("#cpass").val("");
								$("#pass").val("");
								$("#opass").val("");
					} else {
						alert('Error : INTERNAL SERVER ERROR!');
					}
				});
				

		} else  {
			alert(error);
		}

	});

	$(".user_type").click(function(){

				if($(this).val() > 0) {

					var data = {"user_id":$(this).val(),"action":"assign_role"};
					ajaxQuery('../controller/register_controller.php',data).done(function(request){

						if(request==1){
							alert('User Role Successfully Changed!');
							window.location.href='?pages=users_management';
						} else {
							alert("Error: Internal Server Error!");
						}
					});


				} else {

					alert('Error: Invalid Entry Value');
				}


	});

	// purchases 
	// $("#routes").change(function(){
	// 	var routes = $(this).val();
	// 	if(routes)
	// 	{
	// 		// fetch cost / time
	// 		var data = {"data":routes,"action":"api_time_cost"};
	// 		ajaxQuery('../controller/routes_controller.php',data).done(function(request){
	// 			if(request){
	// 				var result =  request.split('/');
	// 				if(result)
	// 				{
	// 						$("#cost").html(numberWithCommas(result[0]));
	// 						$("#time").html(result[1]);
	// 						var payment = parseInt(result[0]) + parseInt($('#price').val());
	// 						$("#payment").val(payment);
	// 						$("#text_payment").html(numberWithCommas(payment));
	// 				}
	// 			}
	// 		});
	// 	} else {
	// 						$("#cost").html("");
	// 						$("#time").html("");
	// 						$("#payment").val("");
	// 						$("#text_payment").html("");
	// 	}
	// });

	$("#delivery_btn").click(function (){

		var error ='';
		if(!$("#routes").val()) {
			error += 'Error : Please Select a Route!'+'\n';
		}
		if(!$("#payment").val()) {
			error += 'Error : Payment cannot be empty!'+'\n';
		}
		if(!$("#product_id").val()){
			error += 'Error : Invalid data Insertion!'+'\n';
		}
		if(!$("#o_to_d").val()){
			error += 'Error : Invalid data Insertion!'+'\n';
		}
		if(!$("#cost").val()){
			error += 'Error : Invalid data Insertion!'+'\n';
		}
		if(!$("#dist").val()){
			error += 'Error : Invalid data Insertion!'+'\n';
		}
		if(!$(this).val()){
			error += 'Error : Invalid data Insertion!'+'\n';
		}

		if(!error){
			
			var r = confirm('Are you sure you want to purchase this product?');
			
			if(r){

				// ajasx send
				var data = {"routes":$("#routes").val(),"payment":$("#payment").val(),"product":$("#product_id").val(),"users":$(this).val(),"otd":$("#o_to_d").val(),"dist":$("#dist").val(),"cost":$("#cost").val(),"action":"save",};
				ajaxQuery('../controller/deliveries_controller.php',data).done(function(request){

					if(request==1){
								alert('Update Successfully');
								window.location.href='?pages=users_purchases_product';
					} else {
						alert('Error : INTERNAL SERVER ERROR!');
					}

				});
			}


		} else {

			alert(error);
		}
	});

});


function ajaxQuery(url,request,method="POST"){
            
     return $.ajax({
                    type: method,
                    url: url,
                    data: {request:request},
                   	cache: false
                     });
    }