<?php
	if(isset($_SESSION['user_type']) && $_SESSION['user_type']==1 && $_SESSION['login']){ 
?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="?pages=admin">ADMIN</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Routes
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="?pages=create_new_routes">Create New Routes</a>
          <a class="dropdown-item" href="?pages=show_routes">Routes Dashboard</a>
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Product
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="?pages=create_product">Create New Product</a>
          <a class="dropdown-item" href="?pages=show_product">Product Dashboard</a>
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Deliveries
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="?pages=deliveries">View Deliveries</a>
        </div>
      </li>
      <?php if($users_info['acc_ex']==1) { ?>
      <li class="nav-item">
        <a class="nav-link" href="?pages=users_management">Users</a>
      </li>
      <?php } ?> 
      <li class="nav-item">
        <a class="nav-link" href="?pages=users_account">My Account</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="?pages=__logout">Log Out</a>
      </li>

    </ul>
  </div>
</nav>

<?php
 }
 ?>