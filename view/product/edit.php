<?php
	if(isset($_SESSION['user_type']) && $_SESSION['user_type']==1 && $_SESSION['login'] && $_GET['pages']=='edit_product' && $_GET['id']!=""){ 
?>
		<div style="margin-bottom:20px; margin-top: 20px;">
				<h1> Edit Product </h1>
		</div>
		<div style="padding:10px;">

		<form id="new_product" style="width: 50%;">

			<div class="form-group">
				<label for="product_name">  Product Name  </label>
				<input type="text" name="product_name" id="product_name" class="form-control" value="<?php echo $name; ?>">
			</div>

			<div class="form-group">
				<label for="product_type">  Product Type  </label>
				<select name="product_type" id="product_type" class="form-control">

					<option value=""> --Select Product Type  </option>
					<option value="shoes" <?php echo $type=='shoes' ? 'selected':''; ?>> Shoes  </option>
					<option value="computer"  <?php echo $type=='computer' ? 'selected':''; ?>> Computer  </option>
					<option value="bags"  <?php echo $type=='bags' ? 'selected':''; ?>> Bags  </option>
					<option value="accessories"  <?php echo $type=='accessories' ? 'selected':''; ?>> Accessories  </option>


				</select>
			</div>

			<div class="form-group">
				<label for="cost">  Product Cost  </label>
				<input type="text" name="cost" id="cost" class="form-control" value="<?php echo $cost; ?>">
			</div>

			<div class="form-group">
				<label for="origin">  Product Origin  </label>
				<select name="origin" id="origin" class="form-control">
					<?php
						while($rows = $destination->fetch_assoc())
						{
							
							?>

								<option value="<?php echo $rows['id'];?>" <?php echo ($rows['id']==$destination_id) ? 'selected' : ''; ?>> <?php echo $rows['country_name'] ?> </option>

							<?php
						}

					?>

				</select>
			</div>


			<input type="hidden" name="id" id='product_id' value="<?php echo $id ?>">
			<button id="product_edit_btn" class="btn btn-success"> Update </button>

		</form>

		</div>


		<div id="error_msg">
		<div id="error1"> Error : Product Name Should not be blank</div>
		<div id="error2"> Error: Product Type not be blank </div>
		<div id="error3"> Error: Product Cost not be blank </div>
		</div>
<?php


 } else
 {
 	header('location:?pages=index_page');
 }

?>