<?php
	if(isset($_SESSION['user_type']) && $_SESSION['user_type']==1 && $_SESSION['login'] && $_GET['pages']=='create_product'){ 
?>

<div style="margin-bottom:20px; margin-top: 20px;">
				<h1> Create New Product </h1>
		</div>
		<div style="padding:10px;">

		<form id="new_routes" style="width: 50%;">

			<div class="form-group">
				<label for="product_name">  Product Name  </label>
				<input type="text" name="product_name" id="product_name" class="form-control">
			</div>

			<div class="form-group">
				<label for="product_type">  Product Type </label>
				<select name="product_type" id="product_type" class="form-control">

					<option value=""> --Select Product Type  </option>
					<option value="shoes"> Shoes  </option>
					<option value="computer"> Computer  </option>
					<option value="bag"> Bags  </option>
					<option value="accessories"> Accessories  </option>

				</select>
			</div>

			<div class="form-group">
				<label for="cost">  Product Cost  </label>
				<input type="text" name="cost" id="cost" class="form-control">
			</div>
			<div class="form-group">
				<label for="origin">  Product Origin  </label>
				<select name="origin" id="origin" class="form-control">
					<option value=""> -- SELECT PRODUCT ORIGIN  </option>
					<?php

						while($rows = $destination->fetch_assoc())
						{
							extract($rows);
							?>

								<option value="<?php echo $id;?>"> <?php echo $country_name ?> </option>

							<?php
						}

					?>

				</select>
			</div>

			<button id="product_btn" class="btn btn-success"> Save </button>

		</form>

		</div>


		<div id="error_msg">
		<div id="error1"> Error : Product Name Should not be blank</div>
		<div id="error2"> Error: Product type not be blank </div>
		<div id="error3"> Error: Product Cost not be blank </div>
		</div>


<?php


 } else
 {
 	header('location:?pages=index_page');
 }

?>