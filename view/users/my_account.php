<?php
	if(isset($_SESSION['user_type']) && ($_SESSION['user_type']==2 || $_SESSION['user_type']==1) && $_SESSION['login'] && $_GET['pages']=='users_account'){

?>

	<div class="container" style="margin-top: 10px;">
		
		<h1> MY ACCOUNT </h1>
		<div style="margin-top: 10px; width: 50%;">
			
			<div class="form-group">
				<label for="fname">  Fullname </label>
				<input type="text" name="fname" id="fname" class="form-control" value="<?php echo $users_info['fname']; ?>">
			</div>

			<div class="form-group">
				<label for="uname">  Username  </label>
				<input type="text" name="uname" id="uname" class="form-control" value="<?php echo $users_info['username']; ?>">
			</div>

			<div class="form-group">
				<label for="email">  Email Address  </label>
				<input type="text" name="email" id="email" class="form-control" value="<?php echo $users_info['email']; ?>">
			</div>

			<div class="form-group">
				<label for="address">  Complete Address  </label>
				<textarea class='form-control' id='address' name='address'> <?php echo $users_info['address']; ?> </textarea>
			</div>

			<div class="form-group">
				<label for="country">  Country  </label>
				<select id="country" class="form-control">
					<option value=""> --SELECT COUNTRY </option>
					<?php

						while($rows = $destination->fetch_assoc())
						{
							extract($rows);
							?>

								<option value="<?php echo $id;?>" <?php echo ($users_info['country_id']==$id) ? 'selected':''; ?> > <?php echo $country_name ?> </option>

							<?php
						}

					?>
				</select>
			</div>
			<small> If you want to change Password please fill out all password fields. </small>
			<div class="form-group">
				<label for="opass"> Old Password  </label>
				<input type="password" name="opass" id="opass" class="form-control">
			</div>

			<div class="form-group">
				<label for="pass">  New Password  </label>
				<input type="password" name="pass" id="pass" class="form-control">
			</div>

			<div class="form-group">
				<label for="cpass">  Confirmed Password  </label>
				<input type="password" name="cpass" id="cpass" class="form-control">
			</div>
			<button id="edit_account_btn" class="btn btn-danger"> Update </button>

		</div>

	</div>


<?php


 } else
 {
 	header('location:?pages=index_page');
 }

?>