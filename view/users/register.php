<?php
	if($_GET['pages']=='index_register'){ 
?>
		<div style="margin-bottom:20px; margin-top: 20px;">
				<h1> User Registeration </h1>
				<a href="?pages=index_page"> I have Already an Account! </a>
		</div>
		<div style="padding:10px;">

		<form id="new_routes" style="width: 50%;">

			<div class="form-group">
				<label for="fname">  Fullname </label>
				<input type="text" name="fname" id="fname" class="form-control">
			</div>

			<div class="form-group">
				<label for="uname">  Username  </label>
				<input type="text" name="uname" id="uname" class="form-control">
			</div>

			<div class="form-group">
				<label for="email">  Email Address  </label>
				<input type="email" name="email" id="email" class="form-control">
			</div>

			<div class="form-group">
				<label for="address">  Complete Address  </label>
				<textarea class='form-control' id='address' name='address'></textarea>
			</div>

			<div class="form-group">
				<label for="country">  Country  </label>
				<select id="country" class="form-control">
					<option value=""> --SELECT COUNTRY </option>
					<?php

						while($rows = $destination->fetch_assoc())
						{
							extract($rows);
							?>

								<option value="<?php echo $id;?>"> <?php echo $country_name ?> </option>

							<?php
						}

					?>
				</select>
			</div>

			<div class="form-group">
				<label for="pass">  Password  </label>
				<input type="password" name="pass" id="pass" class="form-control">
			</div>

			<div class="form-group">
				<label for="cpass">  Confirmed Password  </label>
				<input type="password" name="cpass" id="cpass" class="form-control">
			</div>

			<button id="register_btn" class="btn btn-success"> Sign Up </button>

		</form>

		</div>

<?php


 } else
 {
 	header('location:?pages=index_page');
 }

?>