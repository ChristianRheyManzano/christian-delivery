<?php
	if(isset($_SESSION['user_type']) && $_SESSION['user_type']==2 && $_SESSION['login'] && $_GET['pages']=='users'){ 
?>

	<div class="container" style="margin-top: 10px;">
			
			<div>
				 <h1> WELCOME USER </h1>
			</div>

			<div class="container-fluid" style="padding: 30px;">
					<div class="row">
						<?php while($rows = $users_pro->fetch_assoc()) {  extract($rows);?>
						<div class="col-md-4">
							<span style="font-size:50px;"> <i class="fas fa-shopping-cart"></i> </span>
							<div>
								<span> <strong> Product Name: </strong> <?php echo $name ?> </span>
							</div> 
							<div>
								<span> <strong> Type: </strong> <?php echo $type ?> </span>
							</div>
							<div>
								<span> <strong> Price: </strong> <?php echo number_format($cost); ?> </span>
							</div>
							<div>
								<a href="?pages=users_purchases&id=<?php echo $id ?>"> Purchased This Item </a>
							</div>
						</div>
					<?php } ?>
					</div>
			</div>

	</div>


<?php


 } else
 {
 	header('location:?pages=index_page');
 }

?>