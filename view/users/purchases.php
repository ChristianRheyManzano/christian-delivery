<?php
	if(isset($_SESSION['user_type']) && $_SESSION['user_type']==2 && $_SESSION['login'] && $_GET['pages']=='users_purchases' && $_GET['id']>0){
		$rows = $users_purchase->fetch_assoc();
		extract($rows);

?>

	<div class="container" style="margin-top: 10px;">
		
		<h3> The Item View /  <?php echo $name; ?> </h3>
		<div class="container-fluid">

			<div> <span style="font-size:100px;"> <i class="fas fa-shopping-cart"></i> </span> </div>
			
			<div> <strong> Product Details </strong> </div>
			<div>
				<ul>
					<li> Name : <?php echo $name; ?> </li>
					<li> Type : <?php echo $type; ?> </li>
					<li> Price: <?php echo number_format($cost); ?> </li>
				</ul>
			</div>

			<div> <strong> Delivery Details </strong> </div>

			<div>
				<ul>
					<li> Product Origin :  <?php echo $dest->dquery($destination_id,'country_name'); ?> </li>
					<li> Your Destination : <?php echo ($users_info['address'])? $users_info['address'].", ".$dest->dquery($users_info['country_id'],'country_name') : $dest->dquery($users_info['country_id'],'country_name');?> </li>
					<li>
							<!-- <select id="routes" name="routes">
							
								<?php 
									$purchase_routes = $routes->routes_index_fetch_multiple($destination_id,$users_info['country_id'],'id');
									if($purchase_routes!='NO ROUTES AVAILABLE'){
										?>
											<option value=""> --SELECT AVAILABLE ROUTES </option>
									<?php
										}
										
									$purchase_routes = explode(',', $purchase_routes);

									foreach ($purchase_routes as $value) {
										if($value=='NO ROUTES AVAILABLE') {   ?> 

											<option value="" disabled selected> NO ROUTES AVAILABLE </option>

											<?php break; }
								?>

									<option value="<?php echo $value; ?>">  <?php echo ($routes->routes_value($value,'name')); ?> </option>

								<?php
									}
								?>

							</select> -->
							<?php echo $routes->routes_index_fetch_multiple($destination_id,$users_info['country_id'],'id')?>
					 </li>
					<li> Total Payment: <span> <?php echo number_format($cost_total+$cost);?> </span> </li>
				</ul>
			</div>

			<div>
		<!-- 		<input type="hidden" name="payment" id='payment'> -->
				<input type="hidden" name="product_id" id='product_id' value="<?php echo $id ?>">
				<input type="hidden" name="routes" id='routes' value="<?php echo $route_name ?>">
				<input type="hidden" name="payment" id='payment' value="<?php echo $cost_total+$cost ?>">
				<input type="hidden" name="o_to_d" id='o_to_d' value="<?php echo $dest->dquery($destination_id,'country_name')." To ". $dest->dquery($users_info['country_id'],'country_name')?>">
				<input type="hidden" name="cost" id='cost' value="<?php echo $cost_total ?>">
				<input type="hidden" name="dist" id='dist' value="<?php echo $route_distance ?>">
				<button id='delivery_btn' class="btn btn-success" value="<?php echo $_SESSION['login_id'];?>"> Purchase Now!  </button>

			</div>

		</div>


	</div>


<?php


 } else
 {
 	header('location:?pages=index_page');
 }

?>