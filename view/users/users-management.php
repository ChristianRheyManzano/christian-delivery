<?php
	if(isset($_SESSION['user_type']) && $_SESSION['user_type']==1 && $_SESSION['login'] && $_GET['pages']=='users_management' && $users_info['acc_ex']==1){ 
?>

	<div class="container" style="margin-top: 10px;">

		<h1> Users Management </h1>

		<div style="margin-top:20px;">

			<table class="table table-hover">

				<thead>
					
					<tr>
						
						<th> No. </th>
						<th> Full Name </th>
						<th> Username </th>
						<th> Email Address	</th>
						<th> Address </th>
						<th> User Type </th>
						<th> Role </th>

					</tr>

				</thead>

				<tbody>
					<?php 
						$count=1;
						 while($rows=$users_management->fetch_assoc()){
						 		extract($rows);
						 	?>
						 	<tr>

						 	<td> <?php echo $count++; ?> </td>
						 	<td> <?php echo $fname; ?> </td>
						 	<td> <?php echo $username; ?> </td>
						 	<td> <?php echo $email; ?> </td>
						 	<td> <?php echo ($address) ? $address.", ".$dest->dquery($country_id,'country_name') : $dest->dquery($country_id,'country_name'); ?> </td>
						 	<td> <?php echo ($user_type==1) ? 'Administration' : 'User'; ?> </td>
						 	<td> 
						 		<input type="checkbox" name="user_type" class='user_type' value="<?php echo $id ?>" title='click to change user role' <?php echo $user_type==1 ? 'checked' :''; ?>> <span title='click to change user role'> <i class="fas fa-key"></i> </span>
						 	</td>

						 	</tr>

						 <?php
						 }
					?>

				</tbody>
				
			</table>
			
		</div>

	</div>
		
<?php


 } else
 {
 	header('location:?pages=index_page');
 }

?>