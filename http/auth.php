<?php
// sessions
session_start();

require 'db_config.php';

class auth{

	public function verified()
	{
		if(!isset($_SESSION['login'])){
			
			if(!preg_match('/index/', $_SERVER['REQUEST_URI'])){
				header('location:?pages=index_page');
			}

		} else {
			$_SESSION['login']=1;
			if(preg_match('/index/', $_SERVER['REQUEST_URI'])){
	
					 		if($_SESSION['user_type']==1){

					 			header("location:?pages=admin");
					 		}
					 		else {
					 			header("location:?pages=users");
					 		}
			}
		}
	}
}

?>