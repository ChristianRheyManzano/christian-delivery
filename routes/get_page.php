<?php
	
	// getpages
	class getpages {

		public function source_link($link){

			switch ($link) {
				case 'index_page':
					return 'layout/login.php';
					break;
				case 'index_register':
					return 'users/register.php';
					break;
				case 'admin':
					return 'admin/index.php';
					break;
				case '__logout':
					return '../http/__logout.php';
					break;
				case 'create_new_routes':
					return 'routes/create.php';
					break;
				case 'show_routes':
					return 'routes/show.php';
					break;
				case 'edit_routes':
					return 'routes/edit.php';
					break;
				case 'delete_routes':
					return 'routes/delete.php';
					break;
				case 'status_routes':
					return 'routes/status.php';
					break;
				case 'create_product':
					return 'product/create.php';
					break;
				case 'show_product':
					return 'product/show.php';
					break;
				case 'edit_product':
					return 'product/edit.php';
					break;	
				case 'status_product':
					return 'product/status.php';
					break;
				case 'users':
					return 'users/index.php';
					break;
				case 'users_purchases':
					return 'users/purchases.php';
					break;
				case 'users_purchases_product':
					return 'users/my_purchases.php';
					break;
				case 'users_account':
					return 'users/my_account.php';
					break;
				case 'users_management':
					return 'users/users-management.php';
					break;
				case 'deliveries':
					return 'deliveries/index.php';
					break;			
				default:
					return '';
					break;
			}
		}

	}

?>