<?php

// includes
include_once('../http/auth.php');
include_once('../model/init_model.php');
include_once('../controller/routes_controller.php');
include_once('../controller/product_controller.php');
include_once('../controller/register_controller.php');
include_once('../controller/destination_controller.php');
include_once('../controller/deliveries_controller.php');

//load classes
$auth = new auth();
$auth->verified();
include_once('get_page.php');
$view_page = new getpages;
$source_link = $view_page->source_link($_GET['pages']);

//show function

// routes
if(preg_match('/routes/',$_GET['pages']) || preg_match('/users/',$_GET['pages'])){
$routes = new routes_controller;
$show_routes = $routes->show();
$edit_routes = $routes->edit(isset($_GET['id']) ? $_GET['id'] : '0');
extract($edit_routes);
}

// product
if(preg_match('/product/',$_GET['pages']) || preg_match('/users/',$_GET['pages'])){
$product = new products_controller;
$show_product = $product->show();
$edit_product = $product->edit(isset($_GET['id']) ? $_GET['id'] : '0');
extract($edit_product);
$users_pro = $product->user_view();
$users_purchase = $product->user_purchase(isset($_GET['id']) ? $_GET['id'] : '');
}

//register
if($_GET['pages']=='index_register'){
	$destination = new destination_controller;
	$destination->destinations();
}

// all class
if($_SESSION['login'] && $_SESSION['login_id']){
	$login_id =  $_SESSION['login_id'];

	$register = new register_controller;
	$users_info = $register->users_info($login_id);
	$register = new register_controller;
	$users_management = $register->show();

	$destination = new destination_controller;
	$destination->destinations();
	$dest = new destination_controller;

	$deliveries = new deliveries_controller;
	$fetch_deliveries = $deliveries->show();
	$fetch_deliveries_admin = $deliveries->show_admin();

	$product = new products_controller;
}


//show function




?>