<?php 
if(isset($_POST['request']) || $_GET['pages']){

	class routes_controller
	{

		public function create($name,$time,$cost)
		{
			

				$routes = new routes;
				$data = $routes->where("destination_id={$name}");
				if(!$data->num_rows){

						$routes->input('destination_id',$name);
						$routes->input('delivery_time',$time);
						$routes->input('cost',$cost);
						// $routes->input('distance',$dist);
						$result = $routes->insert();
						if($result){
							return 1;
						}


				} else {
					return 2;
				}
			
			
		}

		public function show(){

			$routes = new routes;
			// $routes->where("status='active'");
			$fetch = $routes->orderBy('id','DESC');
			return $fetch;

		}

		public function edit($id){

			$routes = new routes;
			$data = $routes->where("id={$id} and (status='active' or status='deactive')");
			$result =  $data->fetch_assoc();
			return $result;
		}

		public function update($id,$name,$time,$cost){

			$routes = new routes;
			$data = $routes->where("destination_id={$name} and id!={$id}");
			if(!$data->num_rows){
						$routes->id($id);
						$routes->input('destination_id',$name);
						$routes->input('delivery_time',$time);
						$routes->input('cost',$cost);
						// $routes->input('distance',$dist);
						$result = $routes->update();
						if($result){
							return 1;
						}
				} else {
					return 2;
				}

		}

		public function delete($id){

			$routes = new routes;
			$data = $routes->where("id='{$id}' and (status='active' or status='deactive')");
			if($data->num_rows){
							
							$routes->id($id);
							$result = $routes->delete();

						if($result){
							return 1;
						}
				} else {
					return 2;
				}
		}

		public function status($id,$status){

			$routes = new routes;
			$data = $routes->where("id='{$id}' and (status='active' or status='deactive')");

			if($data->num_rows){
							
							$routes->id($id);
							$routes->input('status',($status=='active') ? 'deactive' : 'active');
							$result = $routes->update();

						if($result){
							return 1;
						}
				} else {
					return 2;
				}
		}

		public function fetch_routes_destination($origin,$destination){

					$dist = new destination;
					$routes = new routes;

					$data =  $dist->where("id={$origin}");
					$fetch = $data->fetch_assoc();
					$origin  = $fetch['two_name'];

					$data = 	$data =  $dist->where("id={$destination}");
					$fetch = $data->fetch_assoc();
					$destination  = $fetch['two_name'];
					$destination;

					$data = $routes->where("status='active'");

					$result='';
					$countData='';

					while($fetch = $data->fetch_assoc()){
						extract($fetch);
						if(preg_match('/^('.$origin.')+(\W)?(TO)+(\W)?('.$destination.')+$/',$name) || preg_match('/^('.$origin.')+(\W)?(TO)+(\W)?(PH|JP|ES|DE|AU|CN|KR)+(\W)?(TO)+(\W)?('.$destination.')+$/',$name)){
							$result .= ($result) ? ','.$name : $name;
							$countData++;
						}
					}
					if($countData){
						return $result;	
					} else {
						return 'NO ROUTES AVAILABLE';
					}
					
			
			}

		// public function routes_index_fetch_multiple($origin,$destination,$column){

		// 		$dist = new destination;
		// 			$routes = new routes;

		// 			$data =  $dist->where("id={$origin}");
		// 			$fetch = $data->fetch_assoc();
		// 			$origin  = $fetch['two_name'];

		// 			$data = 	$data =  $dist->where("id={$destination}");
		// 			$fetch = $data->fetch_assoc();
		// 			$destination  = $fetch['two_name'];
		// 			$destination;

		// 			$data = $routes->where("status='active'");

		// 			$result='';
		// 			$countData='';

		// 			while($fetch = $data->fetch_assoc()){
		// 				if(preg_match('/^('.$origin.')+(\W)?(TO)+(\W)?('.$destination.')+$/',$fetch['name']) || preg_match('/^('.$origin.')+(\W)?(TO)+(\W)?(PH|JP|ES|DE|AU|CN|KR)+(\W)?(TO)+(\W)?('.$destination.')+$/',$fetch['name'])){
		// 					$result .= ($result) ? ','.$fetch[$column] : $fetch[$column];
		// 					$countData++;
		// 				}
		// 			}
		// 			if($countData){
		// 				return $result;	
		// 			} else {
		// 				return 'NO ROUTES AVAILABLE';
		// 			}

		// }
		public function routes_index_fetch_multiple($origin,$destination,$column){

					$dist = new destination;
					$routes = new routes;
					$product = new products;
					$users = new users;

					$routes_value=array();
					$cost_value=array();

					$read_routes = $routes->where("status='active'");

					while($rows = $read_routes->fetch_assoc()){
						$dist_id = $rows['destination_id'];
						$dist_Name = $dist->where("id={$dist_id}");
						$fetch_dist = $dist_Name->fetch_assoc();
						$routes_value[$fetch_dist['two_name']] = $rows['delivery_time'];
						$cost_value[$fetch_dist['two_name']] = $rows['cost'];
					}
					
					$users_dist = $dist->where("id={$destination}");
					$pro_origin = $dist->where("id={$origin}");
					$users_val = $routes->where("destination_id={$destination}");
					$pro_val =  $routes->where("destination_id={$origin}");


					$users_dist = $users_dist->fetch_assoc();
					$pro_origin = $pro_origin->fetch_assoc();
					$users_val = $users_val->fetch_assoc();
					$pro_val = $pro_val->fetch_assoc();

					$pro_origin = $pro_origin['two_name'];
					$pro_val = $pro_val['delivery_time'];
	
					$users_dist =  $users_dist['two_name'];
					$users_val = $users_val['delivery_time'];

					if(count($routes_value) && count($cost_value)){

						unset($routes_value[$pro_origin]);
						unset($routes_value[$users_dist]);
						$lowest=array();
						$max_highest = max($routes_value);
						$get_highest_array=array_search($max_highest, $routes_value);
						unset($routes_value[$get_highest_array]);

						$xO = array();
						$routes_store= array();
						$get_value='';
						$cost_total = '';
						$count=0;
						$path='';

						while(!$get_value) {
							

							foreach($routes_value as $key => $val){
				
										$xO[$key]=$val;

								} //end foreach

							$routes_store=$xO;
							$lowest[$count]=min($xO);

							$get_lowest = array_keys($xO,$lowest[$count],true);

							$path .= $path ? " -> ".$get_lowest[0] :$get_lowest[0];

							unset($routes_value[$get_lowest[0]]);
							unset($xO);
							$cost_total .= ($cost_total) ? "/".$get_lowest[0] :$get_lowest[0];
							$count++;

							if(count($routes_value)==0)
							{
								echo "Routes : ".$pro_origin." -> ".$path." -> ".$users_dist."<br>"	;
								echo "Distance / Time : ";
								echo array_sum($lowest)+$pro_val+$users_val." hrs <br>";
								$cost_total .= ($cost_total) ? "/".$get_lowest[0] :$get_lowest[0];
								$cost_explode = explode('/',$cost_total);
								foreach ($cost_value as $index => $value) {	
										if(array_search($index, $cost_explode)>-1){
											$x+=$value;
										}
									}
								$GLOBALS['route_name']=$pro_origin." -> ".$path." -> ".$users_dist;
								$GLOBALS['route_distance']=array_sum($lowest)+$pro_val+$users_val;
								$GLOBALS['cost_total'] = ($x+$or_value+$dest_vale);
								echo "Delivery Cost : ".($x+$or_value+$dest_vale);
								$get_value=1;
							}

						} //end of while


					}

		}

		public function routes_value($id,$column){

				// $routes = new routes;

				// $data = $routes->where("id={$id}");
				// $fetch = $data->fetch_assoc();
				// return $fetch[$column];
		}

		public function routes_time_cost($value){

			$routes = new routes;

			$data =  $routes->where("name='{$value}' or id={$value}");
			$fetch = $data->fetch_assoc();
			$result = $fetch['cost']."/".$fetch['delivery_time'];
			return $result;
		}


	}	

	// crud
	if(isset($_POST['request']) && $_POST['request']['action']=='save'){

	require 'controller_init.php';

	$routes = new routes_controller;
	echo $routes->create($_POST['request']['routes_name'],$_POST['request']['delivery_time'],$_POST['request']['cost']);
	}

	if(isset($_POST['request']) && $_POST['request']['action']=='update'){

	require 'controller_init.php';

	$routes = new routes_controller;
	echo $routes->update($_POST['request']['id'],$_POST['request']['routes_name'],$_POST['request']['delivery_time'],$_POST['request']['cost']);
	}

	//fetching API
	if(isset($_POST['request']) && $_POST['request']['action']=='api_time_cost'){

		require 'controller_init.php';

		$routes = new routes_controller;
		echo $routes->routes_time_cost($_POST['request']['data']);

	}




} else {
	header("../view/?pages=index_page");
}
?>