<?php 
if(isset($_POST['request']) || $_GET['pages']){


		class products_controller {


		public function create($name,$type,$cost,$origin)
		{

			$products = new products;
			$data = $products->where("name = '{$name}' and type = '{$type}' and cost = '{$cost}' and destination_id={$origin}");
			if(!$data->num_rows){

						$products->input('name',$name);
						$products->input('type',$type);
						$products->input('cost',$cost);
						$products->input('destination_id',$origin);
						$result = $products->insert();
						if($result){
							return 1;
						}


				} else {
					return 2;
				}

		}

		public function show()
		{

			$products = new products;
			$fetch = $products->orderBy('id','DESC');
			return $fetch;

		}

		public function edit($id)
		{

			$prodcts = new products;
			$data = $prodcts->where("id={$id} and (status='active' or status='deactive')");
			$result =  $data->fetch_assoc();
			return $result;

		}

		public function update($id,$name,$type,$cost,$origin)
		{

			$products = new products;
			$data = $products->where("name = '{$name}' and type = '{$type}' and cost = '{$cost}' and destination_id='{$origin}' and id!='{$id}'");
			if(!$data->num_rows){
						$products->id($id);
						$products->input('name',$name);
						$products->input('type',$type);
						$products->input('cost',$cost);
						$products->input('destination_id',$origin);
						$result = $products->update();
						if($result){
							return 1;
						}
				} else {
					return 2;
				}


		}

		public function delete($id){

			$products = new products;
			$data = $products->where("id='{$id}' and (status='active' or status='deactive')");
			if($data->num_rows){
							
							$products->id($id);
							$result = $products->delete();

						if($result){
							return 1;
						}
				} else {
					return 2;
				}
		}

		public function status($id,$status){

			$products = new products;
			$data = $products->where("id='{$id}' and (status='active' or status='deactive')");

			if($data->num_rows){
							
							$products->id($id);
							$products->input('status',($status=='active') ? 'deactive' : 'active');
							$result = $products->update();

						if($result){
							return 1;
						}
				} else {
					return 2;
				}
		}

		public function user_view()
		{
			$products = new products;
			$fetch = $products->where("status='active'");
			return $fetch;

		}

		public function user_purchase($id)
		{
			$prodcts = new products;
			$data = $prodcts->where("id={$id} and status='active'");
			return $data;

		}

		public function products_value($id,$column){

			$products = new products;

			$data = $products->where("id={$id}");
			$fetch = $data->fetch_assoc();
			return $fetch[$column];
		}

		public function products_time_cost($value){

			$products = new products;

			$data =  $products->where("name='{$value}' or id={$value}");
			$fetch = $data->fetch_assoc();
			$result = $fetch['cost']."/".$fetch['delivery_time'];
			return $result;
		}

		}


		if(isset($_POST['request']) && $_POST['request']['action']=='save'){

		require 'controller_init.php';

		$products = new products_controller;
		echo $products->create($_POST['request']['name'],$_POST['request']['type'],$_POST['request']['cost'],$_POST['request']['origin']);
		}


		if(isset($_POST['request']) && $_POST['request']['action']=='update'){

		require 'controller_init.php';

		$products = new products_controller;
		echo $products->update($_POST['request']['id'],$_POST['request']['name'],$_POST['request']['type'],$_POST['request']['cost'],$_POST['request']['origin']);
		}








} else {
	header("../view/?pages=index_page");
}
?>