<?php 
if(isset($_POST['request']) || $_GET['pages']){

	class deliveries_controller
	{

		public function create($user,$route,$product,$payment,$otd,$dist,$cost)
		{
			$deliveries = new deliveries;
			$users = new users;
			$routes = new routes;
			$products = new products;
			$error='';

			$users_num = $users->where("id='{$user}'");
			$routes_num = $routes->where("id='{$route}'");
			$products_num = $products->where("id='{$product}'");

			if($users_num && $routes_num && $products_num){

				// create
				$deliveries->input("user_id",$user);
				$deliveries->input("o_to_d",$otd);
				$deliveries->input("distance",$dist);
				$deliveries->input("cost",$cost);
				$deliveries->input("route_name",$route);
				$deliveries->input("product_id",$product);
				$deliveries->input("payment",$payment);
				$result=$deliveries->insert();
				return $result;


			} else {

				return "INTERNAL SERVER ERROR";
			}


			
		}

		public function show(){
				$login_id =  $_SESSION['login_id'];
				$deliveries = new deliveries;
				$deliveries->where("user_id=$login_id");
				$fetch = $deliveries->orderBy('id','DESC');
				return $fetch;
		}

		public function edit($id){

		}

		public function update($id,$name,$time,$cost,$dist){


		}

		public function delete($id){


		}

		public function show_admin(){

				$deliveries = new deliveries;
				$deliveries->read();
				$fetch = $deliveries->orderBy('id','DESC');
				return $fetch;
		}
		

	}	

	if(isset($_POST['request']) && $_POST['request']['action']=='save'){

	require 'controller_init.php';

		$deliveries = new deliveries_controller;
		echo $deliveries->create($_POST['request']['users'],$_POST['request']['routes'],$_POST['request']['product'],$_POST['request']['payment'],$_POST['request']['otd'],$_POST['request']['dist'],$_POST['request']['cost']);

	}


} else {
	header("../view/?pages=index_page");
}
?>