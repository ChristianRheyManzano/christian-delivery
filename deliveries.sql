-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.37-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for delivery
CREATE DATABASE IF NOT EXISTS `delivery` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `delivery`;

-- Dumping structure for table delivery.deliveries
CREATE TABLE IF NOT EXISTS `deliveries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `o_to_d` varchar(50) NOT NULL,
  `route_name` varchar(100) NOT NULL,
  `cost` int(11) NOT NULL,
  `distance` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `payment` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table delivery.deliveries: ~0 rows (approximately)
/*!40000 ALTER TABLE `deliveries` DISABLE KEYS */;
INSERT INTO `deliveries` (`id`, `user_id`, `o_to_d`, `route_name`, `cost`, `distance`, `product_id`, `payment`) VALUES
	(1, 2, 'China  To Spain', 'CN -> PH -> JP -> DE -> ES', 22, 45, 2, 45),
	(2, 4, 'Australia To China ', 'AU -> PH -> JP -> DE -> CN', 22, 45, 4, 322);
/*!40000 ALTER TABLE `deliveries` ENABLE KEYS */;

-- Dumping structure for table delivery.destination
CREATE TABLE IF NOT EXISTS `destination` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(50) NOT NULL,
  `two_name` varchar(50) NOT NULL,
  `three_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table delivery.destination: ~7 rows (approximately)
/*!40000 ALTER TABLE `destination` DISABLE KEYS */;
INSERT INTO `destination` (`id`, `country_name`, `two_name`, `three_name`) VALUES
	(1, 'Philippines', 'PH', 'PHI'),
	(2, 'Japan', 'JP', 'JPN'),
	(3, 'Spain', 'ES', 'ESP'),
	(4, 'Germany ', 'DE', 'DEU'),
	(5, 'Australia', 'AU', 'AUS'),
	(6, 'China ', 'CN', 'CHN'),
	(7, 'Korea', 'KR', 'KOR');
/*!40000 ALTER TABLE `destination` ENABLE KEYS */;

-- Dumping structure for table delivery.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` enum('shoes','computer','bag','accessories') NOT NULL,
  `cost` int(11) NOT NULL,
  `status` enum('active','deactive') NOT NULL DEFAULT 'active',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `latest_updation` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table delivery.products: ~0 rows (approximately)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `destination_id`, `name`, `type`, `cost`, `status`, `created_date`, `latest_updation`) VALUES
	(1, '2', 'Dell 5363', 'computer', 500, 'active', '2020-04-17 14:31:39', NULL),
	(2, '6', 'Lenovo 1', 'computer', 23, 'active', '2020-04-17 14:32:29', '2020-04-17 14:39:54'),
	(3, '6', 'Jansports', 'bag', 34, 'active', '2020-04-17 14:39:44', NULL),
	(4, '5', 'Gold Ring', 'accessories', 300, 'active', '2020-04-17 14:40:14', NULL);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Dumping structure for table delivery.routes
CREATE TABLE IF NOT EXISTS `routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `delivery_time` int(11) NOT NULL DEFAULT '0',
  `cost` varchar(50) NOT NULL,
  `status` enum('active','deactive') NOT NULL DEFAULT 'active',
  `created_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `latest_updation` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table delivery.routes: ~0 rows (approximately)
/*!40000 ALTER TABLE `routes` DISABLE KEYS */;
INSERT INTO `routes` (`id`, `destination_id`, `delivery_time`, `cost`, `status`, `created_datetime`, `latest_updation`) VALUES
	(1, 3, 34, '1', 'active', '2020-04-17 14:44:26', NULL),
	(2, 1, 3, '12', 'active', '2020-04-17 14:44:37', NULL),
	(3, 2, 4, '5', 'active', '2020-04-17 14:44:43', NULL),
	(4, 4, 4, '5', 'active', '2020-04-17 14:44:51', NULL),
	(5, 5, 34, '5', 'active', '2020-04-17 14:45:08', NULL);
/*!40000 ALTER TABLE `routes` ENABLE KEYS */;

-- Dumping structure for table delivery.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `country_id` int(11) NOT NULL,
  `address` tinytext,
  `user_type` enum('1','2') NOT NULL DEFAULT '2',
  `acct_ex` int(1) DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table delivery.users: ~4 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `fname`, `username`, `password`, `email`, `country_id`, `address`, `user_type`, `acct_ex`, `active`) VALUES
	(1, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.com', 4, ' Philippines ', '1', 1, 1),
	(2, 'Juan Dela Cruz', 'user1', 'cc03e747a6afbbcbf8be7668acfebee5', 'user1@user.com', 3, '  sss  ', '2', 0, 1),
	(3, 'Jose Rizal', 'user2', 'cc03e747a6afbbcbf8be7668acfebee5', 'user2@user.com', 1, 'Makati City ', '1', 0, 1),
	(4, 'Bill Gates', 'user3', 'cc03e747a6afbbcbf8be7668acfebee5', 'user3@user.com', 6, '', '2', 0, 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
